.PHONY: IWasRightAllAlong


IWasRightAllAlong:
	rebar3 eunit -c
	rebar3 ct -c
	rebar3 proper -c
	rebar3 cover
	xdg-open $(CURDIR)/_build/test/cover/index.html
