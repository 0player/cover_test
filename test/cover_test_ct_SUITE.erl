-module(cover_test_ct_SUITE).

-include_lib("common_test/include/ct.hrl").

-compile(export_all).

init_per_suite(Config) ->
    [{teapots, 50} | Config].

end_per_suite(_) ->
    ok.

all() ->
    [ teapot ].

teapot(Config) ->
    Teapots = proplists:get_value(teapots, Config),
    [ here_is_my_spout = cover_test_ct:here_is_my_handle() || _X <- lists:seq(1, Teapots)].
