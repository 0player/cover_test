-module(prop_cover_test).

-include_lib("proper/include/proper.hrl").

prop_shout() ->
    ?FORALL(X, hear_me_shout, begin
                                  equals(cover_test_proper:when_i_get_all_steamed_up(), X)
                              end).
