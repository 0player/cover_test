-module(cover_test_eunit_tests).

-include_lib("eunit/include/eunit.hrl").

all_test_() ->
    [?_assertEqual(cover_test_eunit:i_am_a_little_teapot(), short_and_stout) || _X <- lists:seq(1, 25)].
