`cover_test`
=====

An amazing contraption to showcase the bugs in rebar3 cover.

Run
-----
```
$ rebar3 eunit -c
$ rebar3 ct -c
$ rebar3 proper -c
$ rebar3 cover
```
